﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Http
{
    public interface IHttpNegociator
    {
        HttpWebRequest Request { get; }
        void Send();
        string Response { get; }
        Exception InnerException { get; }
        bool IsSuccessful { get; }
    }
}
