﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Http
{
    public class HttpNegociator : IHttpNegociator
    {
        /// <summary>
        /// Http method
        /// </summary>
        private string method;
        /// <summary>
        /// Http content type
        /// </summary>
        private string contentType;
        /// <summary>
        /// Target uri
        /// </summary>
        private Uri uri;
        /// <summary>
        /// Post body
        /// </summary>
        private string body;
        /// <summary>
        /// Response Http code
        /// </summary>
        private HttpStatusCode responseCode;
        /// <summary>
        /// Response body
        /// </summary>
        private string response;

        private Exception innerException;

        /// <summary>
        /// Base model
        /// </summary>
        private HttpWebRequest request;

        /// <summary>
        /// User agent
        /// </summary>
        private string userAgent;

        /// <summary>
        /// accepted content type
        /// </summary>
        private string accept;


        /// <summary>
        /// host
        /// </summary>
        private string host;
        /// <summary>
        /// Response should be saved as byte array
        /// </summary>
        private bool binaryResponse = false;

        /// <summary>
        /// Bin response
        /// </summary>
        private Stream bin = null;

        public HttpNegociator(string uri, string method)
        {
            if (string.IsNullOrEmpty(uri)) throw new Exception("URI is empty");
            if (string.IsNullOrEmpty(method)) throw new Exception("Method is empty");

            this.uri = new Uri(uri);
            this.method = method;
            this.contentType = string.Empty;
            WebRequest webRequest = WebRequest.Create(uri);
            request = (HttpWebRequest)webRequest;
        }

        public HttpNegociator(string uri, string method, string contentType) : this(uri, method)
        {
            if (string.IsNullOrEmpty(contentType)) throw new Exception("ContentType is empty");
            this.contentType = contentType;
        }

        public HttpNegociator(string uri, string method, string contentType, string body) : this(uri, method, contentType)
        {
            if (body == null) throw new Exception("Body is null");
            this.body = body;
        }

        /// <summary>
        /// Http method
        /// </summary>
        public string Method { get => method; set { if (string.IsNullOrEmpty(value)) throw new Exception("Method is empty"); this.method = value; } }
        /// <summary>
        /// Http content type
        /// </summary>
        public string ContentType
        {
            get => contentType; set
            {
                if (string.IsNullOrEmpty(value)) throw new Exception("ContentType is empty");
                this.contentType = value;
            }
        }
        /// <summary>
        /// Target uri
        /// </summary>
        public Uri Uri { get => uri; set => uri = value; }
        /// <summary>
        /// Post body
        /// </summary>
        public string Body
        {
            get => body; set
            {
                if (value == null) throw new Exception("Body is null");
                this.body = value;
            }
        }
        /// <summary>
        /// Response Http code
        /// </summary>
        public HttpStatusCode ResponseCode { get => responseCode; }
        /// <summary>
        /// Response body
        /// </summary>
        public string Response { get => response; }
        /// <summary>
        /// Inner exception cached on safe exception handling
        /// </summary>
        public Exception InnerException { get => innerException; }
        /// <summary>
        /// Was it a succesful request ?
        /// </summary>
        public bool IsSuccessful => responseCode == HttpStatusCode.OK;

        /// <summary>
        /// Base model
        /// </summary>
        public HttpWebRequest Request { get => request; }
        public string UserAgent { get => userAgent; set => userAgent = value; }
        public string Accept { get => accept; set => accept = value; }
        public string Host { get => host; set => host = value; }

        /// <summary>
        /// Response should be saved as byte array
        /// </summary>
        public bool BinaryResponse { get => binaryResponse; set => binaryResponse = value; }

        /// <summary>
        /// Bin response
        /// </summary>
        public Stream BinContent { get => bin; }

        /// <summary>
        /// Make http request. In case of success the octet-stream response will be found in the Response member, otherwise details will be provided in the InnerException member
        /// </summary>
        public void Send()
        {

            switch (method)
            {
                case "POST":
                    {
                        try
                        {
                            if (!string.IsNullOrEmpty(contentType))
                                request.ContentType = contentType;
                            request.Method = method;
                            if (!string.IsNullOrEmpty(userAgent))
                                request.UserAgent = userAgent;

                            if (!string.IsNullOrEmpty(accept))
                                request.Accept = accept;
                            using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                            {
                                writer.Write(body);
                                writer.Close();
                            }

                            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                            {

                                responseCode = response.StatusCode;

                                if (response.StatusCode == HttpStatusCode.OK)
                                {
                                    if (binaryResponse)
                                    {
                                        using (Stream reader = response.GetResponseStream())
                                        {

                                            bin = new MemoryStream();
                                            reader.CopyTo(bin);
                                            reader.Flush();


                                            reader.Close();
                                            response.Close();
                                        }
                                    }
                                    else
                                    {
                                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                                        {



                                            string resp = reader.ReadToEnd();
                                            this.response = resp;
                                            reader.Close();
                                            response.Close();
                                        }
                                    }



                                }
                                else
                                {
                                    this.response = string.Empty;
                                    this.bin = null;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            innerException = ex;
                        }
                        return;
                    }
                case "PUT":
                case "GET":
                    {
                        try
                        {

                            if (!string.IsNullOrEmpty(contentType))
                                request.ContentType = contentType;
                            request.Method = method;

                            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                            {

                                responseCode = response.StatusCode;

                                if (response.StatusCode == HttpStatusCode.OK)
                                {
                                    if (binaryResponse)
                                    {
                                        using (Stream reader = response.GetResponseStream())
                                        {

                                            bin = new MemoryStream();
                                            reader.CopyTo(bin);
                                            reader.Flush();


                                            reader.Close();
                                            response.Close();
                                        }
                                    }
                                    else
                                    {
                                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                                        {



                                            string resp = reader.ReadToEnd();
                                            this.response = resp;
                                            reader.Close();
                                            response.Close();
                                        }
                                    }



                                }
                                else
                                {
                                    this.response = string.Empty;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            innerException = ex;
                        }

                        return;
                    }
                default: throw new Exception("Unrecognized method !");
            }
        }

        public void MultiPartPost(Dictionary<string, object> postParameters)
        {
            try
            {
                if (method != "POST")
                {
                    innerException = new Exception("Method not supported"); //Form-data is post specific content-type. 
                    return;
                }
                string formDataBoundary = String.Format("----------{0:N}", Guid.NewGuid());
                string contentType = "multipart/form-data; boundary=" + formDataBoundary;

                byte[] formData = GetMultipartFormData(postParameters, formDataBoundary);

                request.Method = "POST";
                request.ContentType = contentType;
                request.UserAgent = userAgent;
                request.Host = host;
                request.Accept = accept;
                request.CookieContainer = new CookieContainer();
                request.ContentLength = formData.Length;


                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(formData, 0, formData.Length);
                    requestStream.Close();
                }

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {

                    responseCode = response.StatusCode;

                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {



                            string resp = reader.ReadToEnd();
                            this.response = resp;
                            reader.Close();
                            response.Close();
                        }



                    }
                    else
                    {
                        this.response = string.Empty;
                    }
                }

            }
            catch (Exception ex)
            {
                innerException = ex;
            }
        }

        #region internals

        private byte[] GetMultipartFormData(Dictionary<string, object> postParameters, string boundary)
        {
            Stream formDataStream = new System.IO.MemoryStream();
            bool needsCLRF = false;

            foreach (var param in postParameters)
            {
                // Add a CRLF to allow multiple parameters to be added.
                // Skip it on the first parameter, add it to subsequent parameters.
                if (needsCLRF)
                    formDataStream.Write(encoding.GetBytes("\r\n"), 0, encoding.GetByteCount("\r\n"));

                needsCLRF = true;

                if (param.Value is FileParameter)
                {
                    FileParameter fileToUpload = (FileParameter)param.Value;

                    // Add just the first part of this param, since we will write the file data directly to the Stream
                    string header = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\nContent-Type: {3}\r\n\r\n",
                        boundary,
                        param.Key,
                        fileToUpload.FileName ?? param.Key,
                        fileToUpload.ContentType ?? "application/octet-stream");

                    formDataStream.Write(encoding.GetBytes(header), 0, encoding.GetByteCount(header));

                    // Write the file data directly to the Stream, rather than serializing it to a string.
                    formDataStream.Write(fileToUpload.File, 0, fileToUpload.File.Length);
                }
                else
                {
                    string postData = string.Format("--{0}\r\nContent-Disposition: form-data; name=\"{1}\"\r\n\r\n{2}",
                        boundary,
                        param.Key,
                        param.Value);
                    formDataStream.Write(encoding.GetBytes(postData), 0, encoding.GetByteCount(postData));
                }
            }

            // Add the end of the request.  Start with a newline
            string footer = "\r\n--" + boundary + "--\r\n";
            formDataStream.Write(encoding.GetBytes(footer), 0, encoding.GetByteCount(footer));

            // Dump the Stream into a byte[]
            formDataStream.Position = 0;
            byte[] formData = new byte[formDataStream.Length];
            formDataStream.Read(formData, 0, formData.Length);
            formDataStream.Close();

            return formData;
        }
        private static readonly Encoding encoding = Encoding.UTF8;


        #endregion

    }


    /// <summary>
    /// Easy convert to content types
    /// </summary>
    public static class NegociatorContentType
    {
        public static string ApplicationJson => "application/json";
        public static string TextJson => "text/json";
        public static string ApplicattionPDF => "application/pdf";
        public static string ApplicationUrlEncoded => "application/x-www-form-urlencoded";
    }
    /// <summary>
    /// Easy convert to http methodes
    /// </summary>
    public static class NegociatorMethod
    {
        public static string POST => "POST";
        public static string GET => "GET";
        public static string PUT => "PUT";
    }


    public class FileParameter
    {
        public byte[] File { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
        public FileParameter(byte[] file) : this(file, null) { }
        public FileParameter(byte[] file, string filename) : this(file, filename, null) { }
        public FileParameter(byte[] file, string filename, string contenttype)
        {
            File = file;
            FileName = filename;
            ContentType = contenttype;
        }
    }
}
