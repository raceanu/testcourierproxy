﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Integrations.Integration
{
    public interface IIntegrationCredentials
    {
        string UserName { get; }

        string Password { get; }

        string SubscriptionKey { get; }
    }
}
