﻿using ConsoleApp1.Http;
using ConsoleApp1.Integrations.Errors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Integrations.Integration
{
    public interface IIntegrationToken
    {
        IIntegrationCredentials Credentials { get; }
        Tuple<bool, List<IIntegrationError>> Authorize(IHttpNegociator negociator);
        Tuple<bool, List<IIntegrationError>> ValidateToken();
        void BuildToken(IIntegrationCredentials credentials);
        void Authentificate();
        DateTime CreationDate { get; }
        int MinutesHealth { get; }
        bool IsValid();
        string GetToken();
    }
}
