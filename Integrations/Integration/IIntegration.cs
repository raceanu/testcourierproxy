﻿using ConsoleApp1.Integrations.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Integrations.Integration
{
    public interface IIntegration
    {
        int IntegrationID { get; }
        IIntegrationResult Execute(IIntegrationParameters parameters);
        IIntergrationProxy Proxy { get; }
    }
}
