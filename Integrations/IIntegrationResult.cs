﻿using ConsoleApp1.Integrations.Errors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Integrations
{
    public interface IIntegrationResult
    {
        bool Executed { get; }
        object Result { get; }
        List<IIntegrationError> Errors { get; }
    }
}
