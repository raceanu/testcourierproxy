﻿using ConsoleApp1.Integrations.Integration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Integrations.Proxy
{
    public interface IIntergrationProxy
    {
        IIntegration GetIntegration(int integrationID);
        IIntegrationToken GetToken(int integrationID);

        IIntergrationProxy Instance { get; }

        void RegisterIntegration(IIntegration integration, IIntegrationCredentials credentials);

    }
}
