﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Integrations
{
    public interface IIntegrationParameters
    {
        int CommandID { get; }
        object Parameter { get; }
    }
}
