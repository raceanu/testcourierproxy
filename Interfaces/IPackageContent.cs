﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace service.courier.Interfaces
{
    public interface IPackageContent
    {
        ISourceAddress ISender { get; }
        IDestinationAddress IRecipient { get; }
    }
}
