﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace service.courier.Interfaces
{
    public interface IAuthToken
    {
        DateTime ValidationTime { get; }
        int LifeSpan { get; }
        bool IsValid { get; }
        void Authenticate();
    }
}
