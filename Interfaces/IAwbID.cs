﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace service.courier.Interfaces
{
    public interface IAwbID
    {
        string BarCode { get; set; }
    }

    public interface IAwbPickup
    {
        ISourceAddress ISender { get; }
        IDestinationAddress IRecipient { get; }
    }
}
