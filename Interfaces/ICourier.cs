﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace service.courier.Interfaces
{
    public interface ICourier
    {
        string AwbPath { get; }
        string TokenKey { get; }
        ISourceAddress CenterAddress { get; }
        IAwbID SendCourier(IAwbPickup destination);

        void DownloadAwb(IAwbID awbID, string fileName,bool isFullPath=false);

        IAwbID GenerateAwb( IPackageContent content);
        IAwbLog GetAwbLogs(IAwbID awbID);
        ICallCourier GetCourier(IPackageContent content);

    }
}
