﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace service.courier.Interfaces
{
    interface IAccount
    {
        string UserName { get; set; }
        string Password { get; set; }
        bool IsValid();
    }
}
