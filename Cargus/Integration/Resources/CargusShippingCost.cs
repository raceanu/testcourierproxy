﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration.Resources
{
    public class CargusShippingCost
    {
        double BaseCost { get; set; }
        double ExtraKmCost { get; set; }
        double WeightCost { get; set; }
        double InsuranceCost { get; set; }
        double SpecialCost { get; set; }
        double RepaymentCost { get; set; }
        double Subtotal { get; set; }
        double Tax { get; set; }
        double GrandTotal { get; set; }
        public CargusShippingCost()
        {
            BaseCost = 0.0;
            ExtraKmCost = 0.0;
            WeightCost = 0.0;
            InsuranceCost = 0.0;
            SpecialCost = 0.0;
            RepaymentCost = 0.0;
            Subtotal = 0.0;
            Tax = 0.0;
            GrandTotal = 0.0;
        }
    }
}
