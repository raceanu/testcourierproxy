﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration.Resources
{
    public class CargusParcelCodes
    {
        string Code { get; set; }
        int Type { get; set; }
        public CargusParcelCodes()
        {
            Code = null;
            Type = 0;
        }
    }
}
