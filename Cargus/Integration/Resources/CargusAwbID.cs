﻿using service.courier.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration.Resources
{
    class CargusAwbID:IAwbID
    {
        public CargusAwbID(string json)
        {
            this.BarCode = json.Replace("\"", "");
        }
        public string BarCode { get; set; }
    }
}
