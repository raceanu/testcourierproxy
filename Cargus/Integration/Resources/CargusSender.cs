﻿using service.courier.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration.Resources
{
    public class CargusSender : ISourceAddress
    {
        public int LocationId { get; set; }
        public string Name { get; set; }
        public int CountyId { get; set; }
        public string CountyName { get; set; }
        public int LocalityId { get; set; }
        public string LocalityName { get; set; }
        public int StreetId { get; set; }
        public string StreetName { get; set; }
        public string BuildingNumber { get; set; }
        public string AddressText { get; set; }
        public string ContactPerson { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public CargusSender()
        {
            LocationId = 0;
            Name = null;
            CountyId = 0;
            CountyName = null;
            LocalityId = 0;
            LocalityName = null;
            StreetId = 0;
            StreetName = null;
            BuildingNumber = null;
            AddressText = null;
            ContactPerson = null;
            PhoneNumber = null;
            Email = null;
        }
        public bool IsValid()
        {
            if (!(LocationId < 0))
            {
                return false;
            }
            return true;
        }
    }
}
