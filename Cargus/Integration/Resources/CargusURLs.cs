﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration.Resources
{
    public static class CargusURLs
    {
        public static readonly string LOGIN = "https://urgentcargus.azure-api.net/api/LoginUser";
        public static readonly string TOKE_VALIDATION = "https://urgentcargus.azure-api.net/api/TokenVerification";
        public static readonly string PICKUP = "https://urgentcargus.azure-api.net/api/AwbPickup";
        public static readonly string AWBPOST = "https://urgentcargus.azure-api.net/api/Awbs";
        public static readonly string TRACE_AWB = "https://urgentcargus.azure-api.net/api/AwbTrace";
        public static readonly string DOWNLOAD_AWB = "https://urgentcargus.azure-api.net/api/AwbDocuments";

        public static readonly string SEND_COURIER = "https://urgentcargus.azure-api.net/api/Orders";

        public static readonly string DOWNLOAD_ORDER = "https://urgentcargus.azure-api.net/api/PDF/OrderDocuments";
    }
}
