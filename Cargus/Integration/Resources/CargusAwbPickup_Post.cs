﻿using Newtonsoft.Json;
using service.courier.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration.Resources
{
    public class CargusAwbPickup_Post : IAwbPickup
    {

        public string PickupStartDate { get; set; }
        public string PickupEndDate { get; set; }
        public int Parcels { get; set; }
        public int Envelopes { get; set; }
        public int TotalWeight { get; set; }
        public int ServiceId { get; set; }
        public double DeclaredValue { get; set; }
        public bool OpenPackage { get; set; }
        public int PriceTableId { get; set; }
        public int ShipmentPayer { get; set; }
        public bool SaturdayDelivery { get; set; }
        public bool MorningDelivery { get; set; }
        public string Observations { get; set; }
        public string PackageContent { get; set; }
        public string CustomString { get; set; }
        public string BarCode { get; set; }
        public string ValidationDate { get; set; }
        public string Status { get; set; }
        public string SenderReference1 { get; set; }
        public string RecipientReference1 { get; set; }
        public string RecipientReference2 { get; set; }
        public string InvoiceReference { get; set; }
        [JsonIgnore]
        public ISourceAddress ISender { get; private set; }
        [JsonIgnore]
        public IDestinationAddress IRecipient { get; private set; }

        public CargusSender Sender { get => sender; set { sender = value; ISender = value; } }
        public CargusRecipient Recipient { get => recipient; set { recipient = value; IRecipient = value; } }

        [JsonIgnore]
        CargusSender sender;
        [JsonIgnore]
        CargusRecipient recipient;

        public CargusAwbPickup_Post()
        {
            PickupStartDate = null;
            PickupEndDate = null;

            Sender = null;
            Recipient = null;
            Parcels = 0;
            Envelopes = 0;
            TotalWeight = 0;
            ServiceId = 0;
            DeclaredValue = 0;

            OpenPackage = false;
            PriceTableId = 0;
            ShipmentPayer = 0;

            SaturdayDelivery = false;
            MorningDelivery = false;
            Observations = null;
            PackageContent = null;
            CustomString = null;
            BarCode = null;
            ValidationDate = null;
            Status = null;
            SenderReference1 = null;
            RecipientReference1 = null;
            RecipientReference2 = null;
            InvoiceReference = null;
        }

        public bool IsValid()
        {
            if (String.IsNullOrEmpty(PickupStartDate))
            {
                return false;
            }
            if (String.IsNullOrEmpty(PickupEndDate))
            {
                return false;
            }
            if (!(Sender != null && Sender.IsValid()))
            {
                return false;
            }
            if (!(Recipient != null && Recipient.IsValid()))
            {
                return false;
            }
            if (!(Parcels > 0))
            {
                return false;
            }
            if (!(TotalWeight > 0 && TotalWeight <= 9999))
            {
                return false;
            }
            if (!(ShipmentPayer == 1 || ShipmentPayer == 2))
            {
                return false;
            }

            //bonus
            //PickupStartDate after PickupEndDate
            if (DateTime.Parse(PickupStartDate) > DateTime.Parse(PickupEndDate))
            {
                return false;
            }
            //PickupStartDate before datetime.now
            if (DateTime.Parse(PickupStartDate) < DateTime.Now)
            {
                return false;
            }

            return true;
        }
    }
}
