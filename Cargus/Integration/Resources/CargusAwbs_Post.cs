﻿using ConsoleApp1.Cargus.Integration.Integration;
using Newtonsoft.Json;
using service.courier.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration.Resources
{
    public class CargusAwbs_Post : IPackageContent
    {
        // public int SenderClientId { get; set; }
        // public int TertiaryClientId { get; set; }
        //  public int TertiaryLocationId { get; set; }
        public int Parcels { get; set; }
        public int Envelopes { get; set; }
        public int TotalWeight { get; set; }
        public int ServiceId { get; set; }
        public double DeclaredValue { get; set; }
        public double CashRepayment { get; set; }
        public double BankRepayment { get; set; }
        public string OtherRepayment { get; set; }
        public int PaymentInstrumentId { get; set; }
        public double PaymentInstrumentValue { get; set; }
        public bool HasTertReimbursement { get; set; }
        public bool OpenPackage { get; set; }
        public int PriceTableId { get; set; }
        public int ShipmentPayer { get; set; }
        public int ShippingRepayment { get; set; }
        public bool SaturdayDelivery { get; set; }
        public bool MorningDelivery { get; set; }
        public string Observations { get; set; }
        public string PackageContent { get; set; }
        public string CustomString { get; set; }
        public string BarCode { get; set; }
        public List<CargusParcelCodes> ParcelCodes { get; set; }
        public string ValidationDate { get; set; }
        public CargusShippingCost ShippingCost { get; set; }
        public string Status { get; set; }
        public string SenderReference1 { get; set; }
        public string RecipientReference1 { get; set; }
        public string RecipientReference2 { get; set; }
        public string InvoiceReference { get; set; }

        [JsonIgnore]
        public ISourceAddress ISender { get; private set; }
        [JsonIgnore]
        public IDestinationAddress IRecipient { get; private set; }

        public CargusSender Sender { get => sender; set { sender = value; ISender = value; } }
        public CargusRecipient Recipient { get => recipient; set { recipient = value; IRecipient = value; } }

        [JsonIgnore]
        CargusSender sender;
        [JsonIgnore]
        CargusRecipient recipient;

        public CargusAwbs_Post()
        {
            //SenderClientId = 0;
            //TertiaryClientId = 1;
            // TertiaryLocationId = 0;
            Sender = null;
            Recipient = null;
            Parcels = 0;
            Envelopes = 0;
            TotalWeight = 0;
            ServiceId = 1;
            DeclaredValue = 0;
            CashRepayment = 0;
            BankRepayment = 0;
            OtherRepayment = null;
            PaymentInstrumentId = 0;
            PaymentInstrumentValue = 0;
            HasTertReimbursement = false;
            OpenPackage = false;
            PriceTableId = 0;
            ShipmentPayer = 0;
            ShippingRepayment = 0;
            SaturdayDelivery = false;
            MorningDelivery = false;
            Observations = null;
            PackageContent = null;
            CustomString = null;
            BarCode = null;
            ParcelCodes = null;
            ValidationDate = "01/01/0001";
            ShippingCost = null;
            Status = null;
            SenderReference1 = null;
            RecipientReference1 = null;
            RecipientReference2 = null;
            InvoiceReference = null;
        }
        public bool IsValid()
        {
            if (!(Sender != null && Sender.IsValid()))
            {
                return false;
            }
            if (!(Recipient != null && Recipient.IsValid()))
            {
                return false;
            }
            if (!(Parcels > 0))
            {
                return false;
            }
            if (!(TotalWeight > 0 && TotalWeight <= 9999))
            {
                return false;
            }
            if (!(ShipmentPayer == 1 || ShipmentPayer == 2))
            {
                return false;
            }
            //if (String.IsNullOrEmpty(PackageContent))
            //{
            //   return false;
            //}
            return true;


        }


    }
}
