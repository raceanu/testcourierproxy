﻿using service.courier.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration.Resources
{
    public class GetCourierCall : ICallCourier
    {
        public GetCourierCall(string response)
        {
            Code = response;
        }
        public string Code { get; set; }
    }
}
