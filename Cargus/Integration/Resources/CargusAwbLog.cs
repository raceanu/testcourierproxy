﻿using service.courier.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration.Resources
{
    public class CargusAwbLog : IAwbLog
    {
        public string Code { get; set; }
        public string Type { get; set; }
        public decimal MeasuredWeight { get; set; }
        public decimal VolumetricWeight { get; set; }
        public string ConfirmationName { get; set; }
        public string Observation { get; set; }
        public List<EventLine> Event { get; set; }
    }

    public class EventLine
    {
        public DateTime Date { get; set; }
        public int EventId { get; set; }
        public string LocalityName { get; set; }
        public string Description { get; set; }
    }
}
