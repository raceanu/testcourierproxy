﻿using ConsoleApp1.Cargus.Integration.Integration;
using ConsoleApp1.Integrations.Integration;
using ConsoleApp1.Integrations.Proxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration.Proxy
{
    public class CargusIntegrationProxy : IIntergrationProxy
    {
        /// <summary>
        /// Private mutex
        /// </summary>
        private static volatile object locker = new object();

        /// <summary>
        /// Singleton private instance
        /// </summary>
        private IIntergrationProxy _cipher = null;

        public IIntergrationProxy Instance
        {
            get
            {
                if (_cipher == null)
                {
                    lock (locker)
                    {
                        if (_cipher == null)
                        {
                            _cipher = new CargusIntegrationProxy();
                        }
                    }
                }
                return _cipher;
            }
        }


        public CargusIntegrationProxy()
        {
            containerToken = new Dictionary<int, IIntegrationToken>();
            containerIntegration = new Dictionary<int, IIntegration>();
            _cipher = this;
        }

        public IIntegration GetIntegration(int integrationID)
        {
            lock (locker)
            {
                return containerIntegration[integrationID];
            }
        }

        public IIntegrationToken GetToken(int integrationID)
        {
            lock (locker)
            {
                return containerToken[integrationID];
            }
        }

        /// <summary>
        /// Internal container. Offers O(1) ~ O(n) access time
        /// </summary>
        private Dictionary<int, IIntegrationToken> containerToken;
        private Dictionary<int, IIntegration> containerIntegration;



        /// <summary>
        /// ??????????
        /// </summary>
        /// <param name="integration"></param>
        /// <param name="credentials"></param>
        public void RegisterIntegration(IIntegration integration, IIntegrationCredentials credentials)
        {
             lock (locker)
            {
                if (containerIntegration.ContainsKey(integration.IntegrationID))
                    containerIntegration[integration.IntegrationID] = integration;
                else
                    containerIntegration.Add(integration.IntegrationID, integration);
                var auth = new CargusIntegrationToken(credentials);
                auth.BuildToken(credentials);

                if (containerToken.ContainsKey(integration.IntegrationID))
                    containerToken[integration.IntegrationID] = auth;
                else
                    containerToken.Add(integration.IntegrationID, auth);

            }

        }
    }
}
