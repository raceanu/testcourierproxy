﻿using ConsoleApp1.Integrations;
using ConsoleApp1.Integrations.Errors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration
{
    public class CargusIntegrationResult : IIntegrationResult
    {
        private bool executed;
        public bool Executed { get => executed; }

        private object result;
        public object Result { get => result; }
        private List<IIntegrationError> errors;
        public List<IIntegrationError> Errors { get => errors; }
        public CargusIntegrationResult(bool executed,object result, List<IIntegrationError> errors)
        {
            this.executed = executed;
            this.result = result;
            this.errors = errors;
        }
        public CargusIntegrationResult()
        {

        }
    }
}
