﻿using ConsoleApp1.Cargus.Integration.Errors;
using ConsoleApp1.Cargus.Integration.Proxy;
using ConsoleApp1.Cargus.Integration.Resources;
using ConsoleApp1.Http;
using ConsoleApp1.Integrations;
using ConsoleApp1.Integrations.Errors;
using ConsoleApp1.Integrations.Integration;
using ConsoleApp1.Integrations.Proxy;
using Newtonsoft.Json;
using service.courier.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration.Integration
{
    class CargusIntegration : IIntegration
    {
 
        private int integrationID;
        public int IntegrationID { get => integrationID; }
        private IIntergrationProxy proxy;
        public IIntergrationProxy Proxy { get => proxy; }
        public IIntegrationResult Execute(IIntegrationParameters parameters)
        {
            IIntegrationResult integrationResult = new CargusIntegrationResult();

            switch (parameters.CommandID)
            {
                case (int)CargusCommands.Generate_AWB:                  
                     integrationResult = new CargusIntegrationResult(true,GenerateAwb((CargusAwbs_Post)parameters.Parameter), new List<IIntegrationError>());
                    break;
                case (int)CargusCommands.DownloadAwb:
                  //  integrationResult = new CargusIntegrationResult(true, DownloadAwb(parameters.Parameter), new List<IIntegrationError>());
                    break;
                case (int)CargusCommands.DownloadBorderou:
                   // integrationResult = new CargusIntegrationResult(true, DownloadBorderou(parameters.Parameter), new List<IIntegrationError>());
                    break;
                case (int)CargusCommands.SendCourier:
                  //  integrationResult = new CargusIntegrationResult(true, SendCourier(parameters.Parameter), new List<IIntegrationError>());
                    break;
                default:
                   // integrationResult = new CargusIntegrationResult(true, DownloadAwb(parameters.Parameter), new List<IIntegrationError>());
                    break;
            }
            return integrationResult;
        }

        private static readonly string _prefix = "token_";
        private static Dictionary<int, string> service = null;
        public static Dictionary<int, string> Service
        {
            get
            {
                if (service == null)
                {
                    Dictionary<int, string> aux = new Dictionary<int, string>();
                    aux.Add(1, "Standard");
                    aux.Add(2, "Matinal");
                    aux.Add(4, "Business Partener");
                    aux.Add(5, "Import Europa");
                    aux.Add(7, "Box 5");
                    aux.Add(8, "Dox 0.5");
                    aux.Add(9, "Box 10");
                    aux.Add(10, "Ship &amp; Go Collect");
                    aux.Add(11, "Ship &amp; Go AM");
                    aux.Add(30, "Collect shop");
                    aux.Add(31, "Pre 12");
                    service = aux;
                }
                return service;
            }
        }
        public CargusIntegration(string path, int key, CargusIntegrationCredentials credentials,CargusIntegrationProxy cargusIntegrationProxy)
        {
            proxy = cargusIntegrationProxy;
            integrationID =  key;
            AwbPath = path;
           

            //CourierManager.Cipher.Add(key, this);
            //var auth = new CargusIntegrationToken(credentials);
            //auth.BuildToken(credentials);
            //

        }
        public CargusIntegration(string path, int key, CargusIntegrationCredentials credentials, CargusSender headquarter, CargusIntegrationProxy cargusIntegrationProxy) : this(path, key, credentials, cargusIntegrationProxy)
        {
            Headquarters = headquarter;
        }
        public string AwbPath { get; set; }
        public string TokenKey { get; private set; }
        public CargusSender Headquarters { get; set; }
        ///public ISourceAddress CenterAddress { get => Headquarters; }???
       
        public CargusAwbID GenerateAwb(CargusAwbs_Post content)
        {
            if (!(content is CargusAwbs_Post)) throw new CargusException("Invalid awb object received!");
            IHttpNegociator negociator = new HttpNegociator(CargusURLs.AWBPOST, NegociatorMethod.POST, NegociatorContentType.ApplicationJson, JsonConvert.SerializeObject(content)); 
            this.Proxy.Instance.GetToken(this.integrationID).Authorize(negociator);
            negociator.Send();
         
            if (!negociator.IsSuccessful) throw new CargusException("Awb error! Request was not made !");
            else
                return new CargusAwbID(negociator.Response);
        }
        public void DownloadAwb(IAwbID awbID, string fileName, bool isFullPath = false)
        {
            if (!(awbID is CargusAwbID)) throw new CargusException("Invalid barcode received for cargus awb!");
            // HttpNegociator negociator = new HttpNegociator(CargusURLs.DOWNLOAD_AWB + "LxvHZksc5s58sKW9uk8eG7_qnWbFISfgijTdnqwJ9TSQ_8Mne3&barCodes=[" + awbID.BarCode + "]", NegociatorMethod.GET);
            HttpNegociator negociator = new HttpNegociator(CargusURLs.DOWNLOAD_AWB + "?barCodes=" + awbID.BarCode + "&type=PDF", NegociatorMethod.GET, NegociatorContentType.ApplicattionPDF);
            CargusIntegrationProxy cargusIntegrationProxy = new CargusIntegrationProxy();
            cargusIntegrationProxy.Instance.GetToken(this.integrationID).Authorize(negociator);
            negociator.Send();

            if (negociator.IsSuccessful)
            {
                string path;

                if (isFullPath)
                    path = fileName;
                else
                    path = System.IO.Path.Combine(AwbPath, fileName);


                using (var fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    byte[] bytes = Convert.FromBase64String(negociator.Response.Replace("\"", ""));
                    fs.Write(bytes, 0, bytes.Length);

                }


            }
            else throw negociator.InnerException;
        }
        public IAwbLog GetAwbLogs(IAwbID awbID)
        {
            if (!(awbID is CargusAwbID)) throw new CargusException("Invalid barcode received for cargus awb!");

            HttpNegociator negociator = new HttpNegociator(CargusURLs.TRACE_AWB + "?barCode=" + awbID.BarCode, NegociatorMethod.GET);
            CargusIntegrationProxy cargusIntegrationProxy = new CargusIntegrationProxy();
            cargusIntegrationProxy.Instance.GetToken(this.integrationID).Authorize(negociator);
            negociator.Send();

            if (negociator.IsSuccessful)
            {
                try
                {
                    var list = JsonConvert.DeserializeObject<List<CargusAwbLog>>(negociator.Response);
                    if (list.Count == 0)
                        return null;
                    else
                        return list[0];
                }
                catch (Exception ex)
                {
                    throw new CargusException("Cargus awb history message cannot be parsed! Please call an admin.");
                }

            }
            else throw negociator.InnerException;
        }
        public IAwbID SendCourier(IAwbPickup awb)
        {
            if (!(awb is CargusAwbPickup_Post)) throw new CargusException("Invalid pickup object received!");


            HttpNegociator negociator = new HttpNegociator(CargusURLs.PICKUP, NegociatorMethod.POST, NegociatorContentType.ApplicationJson, JsonConvert.SerializeObject(((CargusAwbPickup_Post)awb)));
            CargusIntegrationProxy cargusIntegrationProxy = new CargusIntegrationProxy();
            cargusIntegrationProxy.Instance.GetToken(this.integrationID).Authorize(negociator);
            negociator.Send();
            if (!negociator.IsSuccessful) throw new CargusException("Pickup error! Request was not made !");
            else return new CargusAwbID(negociator.Response);
        }
        public ICallCourier GetCourier(IPackageContent content)
        {


            CargusSendCourier sendCourier = (CargusSendCourier)content;

            string url = CargusURLs.SEND_COURIER + "?locationId=0&PickupStartDate=" + sendCourier.PickupDate + "&PickupEndDate=";
            url += sendCourier.PickupEndDate + "&action=" + sendCourier.Action;

            HttpNegociator negociator = new HttpNegociator(url, NegociatorMethod.PUT);
            CargusIntegrationProxy cargusIntegrationProxy = new CargusIntegrationProxy();
            cargusIntegrationProxy.Instance.GetToken(this.integrationID).Authorize(negociator);
            negociator.Request.ContentLength = 0;
            negociator.Send();
            if (!negociator.IsSuccessful) throw new CargusException("Pickup error! Request was not made !");
            else return new GetCourierCall(negociator.Response);

        }
        public void DownloadBorderou(ICallCourier call, string fileName, bool isFullPath = false)
        {

            GetCourierCall courierCall = (GetCourierCall)call;

            HttpNegociator falseNegociator = new HttpNegociator(CargusURLs.DOWNLOAD_ORDER, NegociatorMethod.PUT, NegociatorContentType.ApplicationJson);
            CargusIntegrationProxy cargusIntegrationProxy = new CargusIntegrationProxy();
            cargusIntegrationProxy.Instance.GetToken(this.integrationID).Authorize(falseNegociator);

            string url = CargusURLs.DOWNLOAD_ORDER + "?Token=" + cargusIntegrationProxy.GetToken(this.integrationID).GetToken().Replace("Bearer ", "") + "&orderId=" + courierCall.Code + "&docType=PDF";

            HttpNegociator negociator = new HttpNegociator(url, NegociatorMethod.GET);
            cargusIntegrationProxy.GetToken(this.integrationID).Authorize(negociator);


            negociator.BinaryResponse = true;
            negociator.Send();

            if (negociator.IsSuccessful)
            {
                string path;

                if (isFullPath)
                    path = fileName;
                else
                    path = System.IO.Path.Combine(AwbPath, fileName);


                using (var fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    negociator.BinContent.Position = 0;
                    fs.Position = 0;
                    negociator.BinContent.CopyTo(fs);
                    fs.Flush();

                }


            }
            else throw negociator.InnerException;
        }

    }
}
