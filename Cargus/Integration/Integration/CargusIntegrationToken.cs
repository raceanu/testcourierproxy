﻿using ConsoleApp1.Cargus.Integration.Errors;
using ConsoleApp1.Cargus.Integration.Resources;
using ConsoleApp1.Http;
using ConsoleApp1.Integrations.Errors;
using ConsoleApp1.Integrations.Integration;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration.Integration
{
    public class CargusIntegrationToken : IIntegrationToken
    {
        public int MinutesHealth { get; }

        public DateTime CreationDate { get; }

        public IIntegrationCredentials Credentials { get => credentials; }


        /// <summary>
        /// Should not be used!Ensure token validity
        /// </summary>
        /// <returns>Token validity</returns>
        public bool IsValid()
        {
            HttpNegociator negociator = new HttpNegociator(CargusURLs.TOKE_VALIDATION, NegociatorMethod.GET);
            negociator.Request.Headers.Add("Ocp-Apim-Subscription-Key", credentials.SubscriptionKey);
            negociator.Request.Headers.Add("Authorization", token);
            negociator.Send();

            return negociator.IsSuccessful;
        }
        public Tuple<bool, List<IIntegrationError>> Authorize(IHttpNegociator negociator)
        {
            if (credentials == null)
                throw new CargusTokenException("Token is not an instance");
            lock (locker)
            {
                if (ValidateToken().Item1)
                {
                    negociator.Request.Headers.Add("Ocp-Apim-Subscription-Key", credentials.SubscriptionKey);
                    negociator.Request.Headers.Add("Authorization", token);
                    return new Tuple<bool, List<IIntegrationError>>(negociator.IsSuccessful, new List<IIntegrationError>());
                }
                else
                    throw new CargusTokenException("Cargus server refuse to authorize your operation!");
            }
        }

        public void BuildToken(IIntegrationCredentials credentials)
        {
            if (!(credentials is CargusIntegrationCredentials))
                throw new CargusTokenException("IServiceCredentials is not CargusCredentials");

            this.credentials = (CargusIntegrationCredentials)credentials;
            if (string.IsNullOrEmpty(this.credentials.UserName) || string.IsNullOrEmpty(this.credentials.Password) || string.IsNullOrEmpty(this.credentials.SubscriptionKey))
                throw new CargusTokenException("Invalid credentials ! Credentials are missing or do not match security criterias");
        }

        public Tuple<bool, List<IIntegrationError>> ValidateToken()
        {
            if (token == null)
                Authentificate();
            if ((this.creationDate - DateTime.Now).TotalMinutes > this.MinutesHealth)
                Authentificate();

            if (!IsValid())
            {
                Authentificate();
                if (!IsValid())
                {
                    throw new CargusTokenException("Cargus server refuse to authorize your operation!");

                }
                else
                    return new Tuple<bool, List<IIntegrationError>>(true, new List<IIntegrationError>());
            }
            else
                return new Tuple<bool, List<IIntegrationError>>(true, new List<IIntegrationError>());
        }
        /// <summary>
        /// Mutex. Used to lock other requests until token is validated
        /// </summary>
        private volatile object locker;
        public CargusIntegrationToken()
        {
            this.MinutesHealth = 10;
            locker = new object();
            credentials = null;
            creationDate = DateTime.Now;
            token = null;
        }
        public CargusIntegrationToken(IIntegrationCredentials credentials) : this()
        {
            //this.MinutesHealth = 10;
            this.credentials = credentials;
            //creationDate = DateTime.Now;
        }
        /// <summary>
        /// Login credentials
        /// </summary>
        private IIntegrationCredentials credentials;
        /// <summary>
        /// Timestamp generated when token was generated
        /// </summary>
        private DateTime creationDate;
        /// <summary>
        /// Actual bearer token.
        /// </summary>
        private string token;
        /// <summary>
        /// Should not be used! Authenticate the user and get the token
        /// </summary>
        public void Authentificate()
        {
            HttpNegociator negociator = new HttpNegociator(CargusURLs.LOGIN, NegociatorMethod.POST, NegociatorContentType.ApplicationJson, JsonConvert.SerializeObject(credentials));
            negociator.Request.Headers.Add("Ocp-Apim-Subscription-Key", credentials.SubscriptionKey);
            negociator.Send();
            creationDate = DateTime.Now;
            if (!negociator.IsSuccessful) throw new CargusException("Login failed!:" + negociator.InnerException.Message);
            else
            {
                token = "Bearer" + " " + "ieJbad9bUTEgAGfSM4PtO-fuVBztlHbRkgly6hPBr7LprUNTeR";

                //token = "Bearer " + negociator.Response.Replace("\"", "");
            }
        }
        public string GetToken()
        {
            return this.token;
        }
    }
}
