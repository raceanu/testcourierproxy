﻿using ConsoleApp1.Integrations.Integration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration.Integration
{
    public class CargusIntegrationCredentials : IIntegrationCredentials
    {
        [JsonIgnore]
        private string userName;
        [JsonIgnore]
        private string password;
        [JsonIgnore]
        private string subscriptionKey;

        public CargusIntegrationCredentials(string userName, string password, string subscriptionKey)
        {
            this.userName = userName;
            this.password = password;
            this.subscriptionKey = subscriptionKey;
        }
        /// <summary>
        /// Username
        /// </summary>
        public string UserName { get => userName; }
        /// <summary>
        /// Password
        /// </summary>
        public string Password { get => password; }
        /// <summary>
        /// Cargus specific parameter. Subsription. Ignored in login json
        /// </summary>
        [JsonIgnore]
        public string SubscriptionKey { get => subscriptionKey; }
    }
}
