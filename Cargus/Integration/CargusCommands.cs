﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration
{
    public enum CargusCommands : int
    {
        Generate_AWB,
        DownloadAwb,
        SendCourier,
        DownloadBorderou
    }
}
