﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration.Errors
{
    public class CargusTokenException : Exception, ICargusException
    {
        public CargusTokenException() : base() { }
        public CargusTokenException(string message) : base(message) { }
    }
    public class CargusException : Exception, ICargusException
    {
        public CargusException() : base() { }
        public CargusException(string message) : base(message) { }
    }
}
