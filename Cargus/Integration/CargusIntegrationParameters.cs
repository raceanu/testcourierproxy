﻿using ConsoleApp1.Integrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1.Cargus.Integration
{
    class CargusIntegrationParameters : IIntegrationParameters
    {
        private int commandID;
        public int CommandID { get => commandID; }
        private object parameter;
        public object Parameter { get => parameter; }

        public CargusIntegrationParameters(int commandId, object parameter)
        {
            this.commandID = commandId;
            this.parameter = parameter;
        }
    }
}
