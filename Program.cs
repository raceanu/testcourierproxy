﻿using ConsoleApp1.Cargus.Integration;
using ConsoleApp1.Cargus.Integration.Integration;
using ConsoleApp1.Cargus.Integration.Proxy;
using ConsoleApp1.Cargus.Integration.Resources;
using ConsoleApp1.Integrations;
using ConsoleApp1.Integrations.Integration;
using service.courier.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void GenerateAwb(CargusIntegrationProxy cargusIntegrationProxy)
        {

            CargusAwbs_Post pack = new CargusAwbs_Post();
            pack.Sender = new CargusSender();
            pack.Sender.LocationId = 0;
            pack.Recipient = new CargusRecipient();
            pack.Recipient.Name = "test";
            pack.Recipient.CountyId = 33;
            pack.Recipient.LocalityId = 161;
            pack.Recipient.StreetId = 0;
            pack.Recipient.StreetName = "Str. Ionescu";
            pack.Recipient.BuildingNumber = "25";
            pack.Recipient.AddressText = "Bl. 129, Sc. 3, Ap. 4";
            pack.Recipient.ContactPerson = "Ion Popescu ";
            pack.Recipient.PhoneNumber = "072111222";
            pack.Recipient.Email = "test@yahoo.com";
            pack.Parcels = 1;
            pack.Envelopes = 0;
            pack.TotalWeight = 1;
            pack.DeclaredValue = 0;
            pack.CashRepayment = 0;
            pack.BankRepayment = 0;
            pack.OtherRepayment = "";
            pack.OpenPackage = true;
            pack.ServiceId = 1;

            pack.ShipmentPayer = 2;
            pack.SaturdayDelivery = true;
            pack.Observations = "Observatii";
            pack.PackageContent = "Colet";
            pack.CustomString = "Serie client";
            pack.SenderReference1 = "Ref 1 exp";
            pack.RecipientReference1 = "Ref 1 dest";
            pack.RecipientReference2 = "Ref 2 dest";
            pack.InvoiceReference = "referinta facturare";
            IIntegrationParameters parameters = new CargusIntegrationParameters(0, pack);
            IIntegrationResult integrationResult = cargusIntegrationProxy.Instance.GetIntegration(1).Execute(parameters);
            IAwbID iAwbId = (CargusAwbID)integrationResult.Result;
            string code = iAwbId.BarCode;
        }
        static void Main(string[] args)
        {
            CargusIntegrationCredentials serviceCredentials = new CargusIntegrationCredentials("zirkon_implant", "attary2000", "5cef687fc2ba438a838eefa674bc6cec");

            CargusIntegrationProxy cargusIntegrationProxy = new CargusIntegrationProxy();
            IIntegration courier = new CargusIntegration("C:\\Users\\Raceanu\\Desktop\\test", 1, (CargusIntegrationCredentials)serviceCredentials, cargusIntegrationProxy);
            cargusIntegrationProxy.RegisterIntegration(courier, serviceCredentials);



            GenerateAwb(cargusIntegrationProxy);
        }
    }
}
